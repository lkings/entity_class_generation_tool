//-----------------------------------------------------------------------
// <copyright file=" system_frontlibrary.cs" company="公司信息">
// * Copyright (C) 2019wjl Enterprises All Rights Reserved
// * version : 4.0.30319.42000
// * author  : Xal - wjl generated by T4
// * FileName: system_frontlibrary.cs
// * history : Created by T4 06/11/2019 11:06:45
// </copyright>
//-----------------------------------------------------------------------
using System; 
namespace GxKmAdmin.Data_sharing_exchange.Model
{
    /// <summary> 
	/// 数据表备注： [前置库管理]
	/// 数据表    ： system_frontlibrary 
    /// </summary>   
    public partial class system_frontlibrary
    {
      /// <summary>
        /// 
        /// </summary> 
        public string gid { get; set; }
   
        /// <summary>
        /// 
        /// </summary>   
        public DateTime? createTime { get; set; }
   
        /// <summary>
        /// 
        /// </summary>   
        public DateTime? updateTime { get; set; }
   
        /// <summary>
        /// 
        /// </summary>   
        public bool deleteState { get; set; }
   
        /// <summary>
        /// 业务部门名称
        /// </summary>   
        public string userDepartment { get; set; }
   
        /// <summary>
        /// ip地址
        /// </summary>   
        public string ipaddress { get; set; }
   
        /// <summary>
        ///  端口号
        /// </summary>   
        public int dbPortnumber { get; set; }
   
        /// <summary>
        /// 前置库名称
        /// </summary>   
        public string dbName { get; set; }
   
        /// <summary>
        /// 前置库账号
        /// </summary>   
        public string dbUsername { get; set; }
   
        /// <summary>
        /// 前置库密码
        /// </summary>   
        public string dbPssword { get; set; }
   
        /// <summary>
        /// 备注
        /// </summary>   
        public string remark { get; set; }
		
		/// <summary>
        /// 默认字段初始化
        /// </summary>   
		public  system_frontlibrary(){
			this.gid = Guid.NewGuid().ToString("N").ToUpper();
			this.updateTime = DateTime.Now;
			this.createTime = DateTime.Now;
			this.deleteState = true;
		}
		


    }
}
