	//----------table1开始----------
    
	using System;
	namespace MyProject.Entities 
	{
        /// <summary>
        /// 数据表实体类：table1 
        /// </summary>
        [Serializable()]
        public class table1
        {    
		                 
			/// <summary>
			/// Int32:主键
			/// </summary>                       
			public Int32 id {get;set;}   
                         
			/// <summary>
			/// String:姓名
			/// </summary>                       
			public String name {get;set;}   
                         
			/// <summary>
			/// DateTime:创建时间
			/// </summary>                       
			public DateTime createTime {get;set;}   
               
        }    
     }

	//----------table1结束----------

	